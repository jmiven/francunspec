# OCabot

IRC bot for "#OCaml" on freenode

## Build

- `opam pin add calculon https://github.com/c-cube/calculon.git`
- `make`
- `./ocabot.native`

## License

MIT, see `LICENSE`

