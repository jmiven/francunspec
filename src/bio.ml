open Calculon

let cmd_hello : Command.t =
  Command.make_simple ~descr:"bio" ~prefix:"bio" ~prio:10
    (fun (input_msg:Core.privmsg) _ ->
       Lwt.return (Some (String.concat " " [
         "En l'an de grâce 2016, vpm tua sauvagement francinet ;";
         "francunspec est un imposteur qui se fait passer pour la réincarnation de francinet.";
         "Il changera de nom lorsqu'il pourra ne faire que de l'IPv6 lorsqu'il s'appelle francinet4 et que de l'IPv4 lorsqu'il s'appelle francinet6.";
       ])
      )
    )

let plugin = Plugin.of_cmd cmd_hello
