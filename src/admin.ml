open Calculon

module AddrSet = Set.Make (
  struct
    type t = string
    let compare = compare
  end
)

type connectee_mode =
  | Voice
  | Op

type connectee = {
  nick : string;
  user : string option;
  host : string option;
  first_seen : float;
  modes : connectee_mode list;
}

type channel = {
  name : string;
  connectees : (string, connectee) Hashtbl.t;
}

type state = {
  tor_exit_addresses : AddrSet.t Lwt_react.signal;
  channels : (string, channel) Hashtbl.t;
}

module Tor = struct
  let list_fmap f l =
    let rec aux acc = function
      | hd :: tl ->
          begin
            match f hd with
            | Some e -> aux (e :: acc) tl
            | None -> aux acc tl
          end
      | [] ->
          List.rev acc
    in
    aux [] l

  let getnameinfo ip =
    match Unix.inet_addr_of_string ip with
    | inet_addr ->
        begin
          let sockaddr = Unix.ADDR_INET (inet_addr, 0) in
          match%lwt Lwt_unix.getnameinfo sockaddr [] with
          | { Unix.ni_hostname } ->
              Lwt.return ni_hostname
          | exception Not_found ->
              Lwt.return ip
        end
    | exception Failure _ -> (* not an IP so already reversed *)
        Lwt.return ip

  let parse text =
    let open Re_pcre in
    let rex = regexp "^ExitAddress ([^ ]+) " in
    let set = ref AddrSet.empty in
    let lines = split ~rex:(regexp "\n") text in
    let%lwt _nothing =
      lines |> Lwt_list.iter_p (fun line ->
      match extract ~rex line with
      | [| _line; host |] ->
          let%lwt addr = getnameinfo host in
          set := AddrSet.add addr !set;
          Lwt.return_unit
      | _ ->
          Lwt.return_unit
      | exception _ ->
          Lwt.return_unit
    )
    in
    Lwt.return !set

  let exit_addresses_get _ipset () =
    let open Cohttp in
    let open Cohttp_lwt_unix in
    let uri = "https://check.torproject.org/exit-addresses" in
    let%lwt resp, body = Client.get (Uri.of_string uri) in
    let code = resp |> Response.status |> Code.code_of_status in
    Printf.printf "Response code: %d\n" code;
    let%lwt body = Cohttp_lwt.Body.to_string body in
    parse body

  let exit_addresses () =
    let trigger =
      let first = ref true in
      fun () ->
        if !first then
          Lwt.return (first := false)
        else
          Lwt_unix.sleep 3600.
    in
    Lwt_react.E.from trigger
    |> Lwt_react.S.fold_s exit_addresses_get AddrSet.empty
end

let default_state () =
  {
    channels = Hashtbl.create 10;
    tor_exit_addresses = Tor.exit_addresses ();
  }

let connectee ~nick ~user ~host ~first_seen ~modes = {
  nick; user; host; first_seen; modes;
}

let find_channel { channels } chan =
  match Hashtbl.find channels chan with
  | chan -> chan
  | exception Not_found ->
      let chan = { name = chan; connectees = Hashtbl.create 10 } in
      Hashtbl.add channels chan.name chan;
      chan

let send_raw (module C : Core.S) message=
  C.I.send ~connection:C.connection message

module Shopping = struct
  module type Costs = sig
    type t
    val budget : t
    val zero : t
    val cost_function : string -> t
    val add : t -> t -> t
    val compare : t -> t -> int
  end
  module IRC_Modes : Costs = struct
    type t = {
      count : int;
      length : int;
    }

    let budget = {
      count = 12; (* XXX *)
      length = 400; (* XXX *)
    }

    let zero = {
      count = 0;
      length = 0;
    }

    let cost_function nick = {
      count = 1;
      length = String.length "v" + String.length nick;
    }

    let add a b = {
      count = a.count + b.count;
      length = a.length + b.length;
    }

    let compare a b =
      max (compare a.count b.count) (compare a.length b.length)
  end

  let bucket ~costs:(module Costs : Costs) ~lst =
    let rec aux acc current_cost = function
      | (hd :: tl) as l ->
          let cost = Costs.add current_cost (Costs.cost_function hd) in
          if Costs.compare cost Costs.budget > 0 then
            acc, l
          else
            aux (hd :: acc) cost tl
      | [] as tl ->
          acc, tl
    in
    aux [] Costs.zero lst

  let spread ~costs:(module Costs : Costs) ~lst =
    let rec aux acc lst =
      let bucket, rest = bucket ~costs:(module Costs) ~lst in
      let acc' = bucket :: acc in
      if rest = [] then
        acc'
      else
        aux acc' rest
    in
    aux [] lst
end

let voice (module C : Core.S) ~chan =
  let connectees = Hashtbl.fold (fun _k d acc -> d.nick :: acc) chan.connectees [] in
  Shopping.(spread ~costs:(module IRC_Modes : Costs) ~lst:connectees)
  |> Lwt_list.iter_s (fun nicks ->
      let n = List.length nicks in
      let plus_v = "+" ^ (String.make n 'v') in
      Irc_message.other ~cmd:"mode" ~params:(chan.name :: plus_v :: nicks)
      |> send_raw (module C : Core.S)
  )

let ban_temporarily (module C : Core.S) ~duration ~chan ~nick ~user ~host =
  Printf.eprintf "--- Banning %s!%s@%s.\n" nick user host;
  let f on =
    let b = if on then "+b" else "-b" in
    let mask = Printf.sprintf "*!%s@%s" user host in
    let%lwt warning = C.send_privmsg
      ~target:nick
      ~message:"You will be voiced soon."
    in
    Irc_message.other ~cmd:"mode" ~params:[ chan.name; b; mask ]
    |> send_raw (module C : Core.S)
  in
  Lwt_timeout.(start @@ create duration (fun () -> ignore @@ f false));
  f true

let warn_if_extra_args (module C : Core.S) ~who ~extra_args =
  if extra_args <> [] then
    C.send_privmsg
      ~target:who
      ~message:"This command takes no additional arguments, ignoring them."
  else
    Lwt.return_unit

let pp_string_option _outchan = function
  | Some s -> Printf.sprintf "%S" s
  | None -> "<none>"

let loop =
  let re_space = Re_pcre.regexp " " in
  fun (module C : Core.S) ~state ~msg ->
    let message = msg.Core.message in
    let who = msg.Core.nick in
    let chan = msg.Core.to_ in
    match Re_pcre.split ~rex:re_space message with
    | "!nouvelle_star" :: extra_args
    | "!voice_all" :: extra_args ->
        Command.Cmd_match (
          let%lwt () = warn_if_extra_args (module C) ~who ~extra_args in
          voice (module C : Core.S) ~chan:(find_channel state chan)
        )
    | "!pastille" :: extra_args ->
        Command.Cmd_match (
          let chan = find_channel state chan in
          let%lwt () =
            Hashtbl.fold (fun _k d acc -> d :: acc) chan.connectees []
            |> Lwt_list.iter_s (fun { nick; user; host; first_seen } ->
              C.send_privmsg
              ~target:who
              ~message:(Printf.sprintf "%S, %a, %a, %f\n" nick pp_string_option user pp_string_option host first_seen)
            )
          in
          warn_if_extra_args (module C) ~who ~extra_args
        )
    | _ ->
        Command.Cmd_skip

let cmd_admin state : Command.t =
  Command.make ~descr:"admin" ~name:"admin" ~prio:10
    (fun (module C : Core.S) (msg : Core.privmsg) ->
      if msg.Core.nick = "adrien" then
        loop (module C) ~state ~msg
      else
        Command.Cmd_skip
    )

module ChannelUsers = struct
  let parse_user =
    (* example: francunspec!ocabot@80.12.58.25 *)
    let rex = Re_pcre.regexp "([^!]+)!([^@]+)@(.*)" in
    fun s ->
      match Re_pcre.extract ~rex s with
      | [| _s; nickname; user; host |] -> Some (nickname, Some user, Some host)
      | _ -> None

  let with_parsed_user ~user ~f ~g =
    match parse_user user with
    | None -> g ()
    | Some (nick, user, host) -> f ~nick ~user ~host

  let add ~state ~chan:{ connectees } ~nick ~user ~host =
    let tor_exit_addresses = React.S.value state.tor_exit_addresses in
    (match host with
    | Some host when AddrSet.mem host tor_exit_addresses ->
        Printf.eprintf "TOR: %S [%S] uses Tor\n%!" nick host
    | Some host ->
        Printf.eprintf "TOR: %S [%S] does not use Tor\n%!" nick host
    | None ->
        Printf.eprintf "TOR: %S [unknown]\n%!" nick);
    let first_seen = Unix.gettimeofday () in
    (match Hashtbl.find connectees nick with
    | x -> x
    | exception Not_found -> connectee ~nick ~user ~host ~first_seen ~modes:[]
    ) |> Hashtbl.replace connectees nick

  let remove ~chan:{ connectees } ~nick =
    match Hashtbl.remove connectees nick with
    | () -> ()
    | exception Not_found -> ()

  let handle_message (module C : Core.S) state { Irc_message.prefix; command } =
    (* 353 RPL_NAMREPLY "<channel> :[[@|+]<nick> [[@|+]<nick> [...]]]"
     * 366 RPL_ENDOFNAMES "<channel> :End of /NAMES list" *)
    let open Irc_message in
    let g () = () in
    match prefix, command with
    | _, Other ("353", _my_nick :: _at :: chan :: name_list) ->
        let chan = find_channel state chan in
        ListLabels.iter name_list ~f:(fun l ->
          Irc_helpers.split ~str:l ~c:' '
          |> List.filter ((<>) "")
          |> List.iter (fun nick -> add ~state ~chan ~nick ~user:None ~host:None)
        );
        Lwt.return_unit
    | _, Other ("366", _sl) ->
        Lwt.return_unit
    | _, Other (id, sl) ->
        Printf.eprintf "NEW MESSAGE: id=%S\n%!" id;
        List.iter (Printf.eprintf "  s: %S\n%!") sl;
        Lwt.return_unit
    | Some prefix, JOIN (chans, _key_list) ->
        let%lwt l = with_parsed_user ~user:prefix ~g:(fun () -> Lwt.return [ Command.Cmd_skip ]) ~f:(fun ~nick ~user ~host ->
          Lwt_list.map_s (fun chan ->
            let chan = find_channel state chan in
            let tor_exit_addresses = React.S.value state.tor_exit_addresses in
            match user, host with
            | Some user, Some host ->
                if AddrSet.mem host tor_exit_addresses || user = "webchat" then
                  Lwt.return @@ Command.Cmd_match (
                    ban_temporarily (module C : Core.S) ~duration:300 ~chan ~nick ~user ~host
                  )
                else
                  Lwt.return @@ Command.Cmd_skip
            | _ ->
                Lwt.return @@ Command.Cmd_skip
          ) chans
        )
        in
        with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user ~host ->
          ListLabels.iter chans ~f:(fun chan ->
            add ~state ~chan:(find_channel state chan) ~nick ~user ~host
          )
        );
        Lwt.return_unit
    | Some prefix, KICK (chans, _ (* by_whom *), _ (* comment *))
    | Some prefix, PART (chans, _ (* comment *)) ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          ListLabels.iter chans ~f:(fun chan ->
            remove ~chan:(find_channel state chan) ~nick
          )
        )
    | Some prefix, QUIT _message ->
        Lwt.return @@ with_parsed_user ~user:prefix ~g ~f:(fun ~nick ~user:_user ~host:_host ->
          Hashtbl.iter
            (fun _chan_name chan -> remove ~chan ~nick)
            state.channels 
        )
    | None, (JOIN _ | KICK _ | PART _ | QUIT _) ->
        Lwt.return_unit
    | _ ->
        prerr_endline "Message not handled";
        Lwt.return_unit
end

let handle_others state (module C : Core.S) irc_message =
  Printf.eprintf "Received %S.\n%!" (Irc_message.to_string irc_message);
  ChannelUsers.handle_message (module C : Core.S) state irc_message

let plugin =
  Plugin.stateful
    ~name:"admin"
    ~commands:(fun state -> [ cmd_admin state ]) ()
    ~on_msg:(fun state -> [ handle_others state ])
    ~to_json:(fun _state -> None)
    ~of_json:(fun _ _ -> Lwt.return (Result.Ok (default_state ())))
